const User = {
  template: `
    <div class="">
      <router-view></router-view>
    </div>
  `
}

const UserHome = { template: 
              `<div class="panel border child">
                <router-link class="back" to="/user">Home</router-link>
                <h1 class="margin_top">Select User</h1>
                <p class="margin_top">1. L.K.Jogn</p>
                <p>2. Peter M.</p>
                <p>3. Victoria S. Malan</p>
                <p>4. Samuel Singh</p>
                <router-link class="margin_top next" to="/user/link/profile">Add User</router-link>                
              </div>` }
const UserProfile = { template: 
              `<div class="panel border child">
                <router-link class="back" to="/user/link">User List</router-link>
                <h1 class="margin_top">Add User</h1>
              </div>` }

const router = new VueRouter({
  routes: [
    { path: '/user/:id', component: User,
      children: [

        { path: '', component: UserHome },

        { path: 'profile', component: UserProfile }
      ]
    }
  ]
})

const app = new Vue({ router }).$mount('#temper-app')